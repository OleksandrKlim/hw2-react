import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Button/Button";
import "./header.scss";
export class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <Button
          class="header__button"
          text={this.props.text}
          context={this.props.like}
        />

        <Button
          class="header__button--buy"
          text={this.props.text2}
          context={this.props.buy}
        />
      </header>
    );
  }
}
Header.propTypes = {
  text: PropTypes.string,
  buy: PropTypes.number,
  class: PropTypes.string,
};
