import React from "react";
import PropTypes from "prop-types";
import "./button.scss";
export class Button extends React.Component {
  render() {
    return (
      <button
        id={this.props.idButton}
        type="button"
        className={this.props.class}
        style={{ backgroundColor: this.props.backgroundColor }}
        onClick={(e) => {
          this.props.handleClick();
        }}
      >
        {this.props.text} {this.props.context}
      </button>
    );
  }
}
Button.propTypes = {
  backgroundColor: PropTypes.object,
  style: PropTypes.object,
  onClick: PropTypes.func,
};
