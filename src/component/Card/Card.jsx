import React from "react";
import PropTypes from "prop-types";
import "./Card.scss";
export class Card extends React.Component {
  render() {
    const { name, img, article, color, prise, action } = this.props;
    return (
      <li className="item">
        <h2 className="item__header"> Назва товару:{name} </h2>
        <div className="item__wrapper">
          <img src={img} alt="{name}" className="item__img" />
        </div>
        <p className="item__prise">Ціна: {prise} грн.</p>
        <p className="item__art">Артікул: {article}</p>
        <p className="item__color">Колір: {color}</p>
        <div>{action}</div>
      </li>
    );
  }
}

Card.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  prise: PropTypes.number.isRequired,
  article: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
