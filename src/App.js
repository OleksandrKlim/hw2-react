import React from "react";
import PropTypes from "prop-types";
import { Modal } from "./component/Modal/Modal";
import { Button } from "./component/Button/Button";
import { Card } from "./component/Card/Card";
import { Header } from "./component/Header/Header";
import { Svg } from "./component/Button/Svg";
import "./App.scss";

class App extends React.Component {
  state = {
    isModalVisibleFirst: false,
    isModalVisibleSecond: false,
    product: [],
    like: [],
    buy: [],
    article: [],
  };
  componentDidMount() {
    fetch("./product/data/data.json")
      .then((r) => r.json())
      .then((data) => {
        this.setState({ product: data });
      });
    this.testLike();
    this.testBuy();
  }
  componentDidUpdate() {
    localStorage.setItem("like", JSON.stringify(this.state.like));
    localStorage.setItem("buy", JSON.stringify(this.state.buy));
  }
  testLike = () => {
    if (localStorage.getItem("like") !== null) {
      this.setState({ like: JSON.parse(localStorage.getItem("like")) });
    }
  };
  testBuy = () => {
    if (localStorage.getItem("buy") !== null) {
      this.setState({ buy: JSON.parse(localStorage.getItem("buy")) });
    }
  };

  render() {
    return (
      <div className="App">
        <Header
          like={this.state.like.length}
          text="Like"
          text2="Basket"
          buy={this.state.buy.length}
        />
        <ul className="wrapper-list">
          {this.state.product.map((prod) => (
            <Card
              prod={prod}
              key={prod.article}
              name={prod.name}
              img={prod.img}
              prise={prod.prise}
              color={prod.color}
              article={prod.article}
              action={
                <>
                  <Button
                    class="App__btn"
                    text="Add to cart"
                    handleClick={() => {
                      this.state.buy.includes(prod.article)
                        ? this.setState({
                            ...this.state,
                            isModalVisibleSecond: true,
                            article: prod.article,
                          })
                        : this.setState({
                            ...this.state,
                            isModalVisibleFirst: true,
                            article: prod.article,
                          });
                    }}
                  />

                  <Button
                    class="App__btn--favorite"
                    text=<Svg
                      color={
                        this.state.like.includes(prod.article) ? "red" : "white"
                      }
                    />
                    handleClick={() => {
                      this.state.like.includes(prod.article)
                        ? this.setState({
                            ...this.state,
                            like: this.state.like.filter((n) => {
                              return n !== prod.article;
                            }),
                          })
                        : this.setState({
                            ...this.state,
                            like: [...this.state.like, prod.article],
                          });
                    }}
                  />
                </>
              }
            />
          ))}
        </ul>
        {this.state.isModalVisibleFirst && (
          <Modal
            header="Add purchase to cart ?"
            text="Click ok to add or cancel if you change your mind"
            wrapperClose={(e) => e.stopPropagation()}
            onClose={() => {
              this.setState({ ...this.state, isModalVisibleFirst: false });
            }}
            action={
              <>
                <Button
                  text="Ok"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      buy: [...this.state.buy, this.state.article],
                      isModalVisibleFirst: false,
                    });
                  }}
                />
                <Button
                  text="Cancel"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      isModalVisibleFirst: false,
                    });
                  }}
                />
              </>
            }
          />
        )}
        {this.state.isModalVisibleSecond && (
          <Modal
            closeButton={true}
            header="This item is already in your shopping cart."
            text="Add another one? Click ok to add or delate to remove the item, if you change your mind click the cross"
            wrapperClose={(e) => e.stopPropagation()}
            onClose={() => {
              this.setState({ ...this.state, isModalVisibleSecond: false });
            }}
            action={
              <>
                <Button
                  text="Ok"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      buy: [...this.state.buy, this.state.article],

                      isModalVisibleSecond: false,
                    });
                  }}
                />
                <Button
                  text="Delete"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      buy: this.state.buy.filter((n) => {
                        return n !== this.state.article;
                      }),
                      isModalVisibleSecond: false,
                    });
                  }}
                />
              </>
            }
          />
        )}
      </div>
    );
  }
}
App.propTypes = {
  text: PropTypes.string,
  state: PropTypes.object,
  handleClick: PropTypes.func,
  onClose: PropTypes.func,
  action: PropTypes.object,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
};
Modal.defaultProps = {
  closeButton: false,
  bakGround: "wrapper",
  wrapper: "modal",
  headerText: "modal__header",
  btvCloth: "modal__cloth",
  bodyText: "modal__bodyText",
};
Button.defaultProps = {
  class: "modal__btn",
};
export default App;
